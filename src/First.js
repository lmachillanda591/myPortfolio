import React, { Component } from 'react';
import {Link} from 'react-router';
import './css/First.css';

import Cite from './Cite';
import Bio from './Bio';
import Work from './Work';
import Footer from './Footer';

import Spain from './assets/flags/Spain.png'
import England from './assets/flags/England.png'

import Linkedin from './assets/social/2.svg'
import Facebook from './assets/social/1.svg'
import Twitter from './assets/social/3.svg'
import Flickr from './assets/social/4.svg'


import './data/global.js'
import data from './data/first.json'

class First extends Component {
  constructor() {
    super();
    this.state = {
      lang: "es",
      greeting: data[0].greeting
    };
  }

  _selectLanguage() {
    return this.props.params.lang === "es" ? data[0] : data[1];
  }

  _iconLanguage() {
    return this.props.params.lang === "es" ? {flag: England, link: "/home/en", alt: "england", msg: "Change language"} : {flag: Spain, link: "/home/es", alt: "spain", msg: "Cambiar el idioma"};
  }

  render() {
    const data = this._selectLanguage()
    const icon = this._iconLanguage()
    return (
      <div className="content-fluid">
        <div className="First-box-left col-xs-12 col-lg-6">
        </div>
        <div className="First-box-right col-xs-12 col-lg-6">
          <Link to={icon.link}><img className="flag" title = {icon.msg} alt={icon.alt} src={icon.flag}/></Link>
          <p className="text-center">{data.title}</p>
          <span> {data.greeting}</span>
          <br/>
          <small>nikko1801@gmail.com</small>
          <br/>
          <a className="social" rel="noopener noreferrer" href="https://www.linkedin.com/in/luis-machillanda-ramos-3b5772b9" title="Linkedin profile" target="_blank">
            <img alt="linkedin" src={Linkedin}/>
          </a>
          <a className="social" rel="noopener noreferrer" href="https://www.flickr.com/photos/131320105@N02/" title="Flickr profile" target="_blank">
            <img alt="Flickr" src={Flickr}/>
          </a>
          <a className="social" rel="noopener noreferrer" href="https://twitter.com/NoSoyMozart" title="Twitter profile" target="_blank">
            <img alt="twitter" src={Twitter}/>
          </a>
          <a className="social" rel="noopener noreferrer" href="https://www.facebook.com/luis.machillanda.39" title="Facebook profile" target="_blank">
            <img alt="facebook" src={Facebook}/>
          </a>
        </div>
        <Cite lang={this.props.params.lang}/>
        <Bio lang={this.props.params.lang}/>
        <Footer/>
        <Work lang={this.props.params.lang}/>
      </div>
    );
  }
}

export default First;

//<a href="http://www.freepik.com/free-vector/business-man-at-the-space_902042.htm">Designed by Freepik</a>