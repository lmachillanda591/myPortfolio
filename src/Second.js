import React, { Component } from 'react';
import './css/Second.css';
import Mocks from './mocks.json';

class Second extends Component {
	constructor() {
		super();
		this.state = {
			showComments: false,
			showButtons: false
		};
	}

	_handleClick() {
    this.setState({
      showComments: !this.state.showComments
    });
  }

  _getElements = () => {
    return Mocks.map( (element) => {
      return (
        <div key={element.id} className={element.class_name}>
          <img className={element.img_class_name} src={require( `./assets/second/${element.imgName}`)} alt={element.altImg}/>
        </div>
      )
    })
  }

  render() {
    const elements = this._getElements();
    return (
      <div className="secondBox">
        {elements}
      </div>
    );
  }
}

export default Second;