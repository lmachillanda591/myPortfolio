import React, { Component } from 'react';
import './css/Work.css';
import data from './data/work.json';

export class Work extends Component {
  _selectLanguage() {
    return this.props.lang === "es" ? data[0] : data[1];
  }

  render() {
    const data = this._selectLanguage();
    var listItems = data.map(function(item) {
      return (
      	<div key={item.id} className="col-xs-12 col-sm-6 col-md-4 text-center workBoxs">
      		<a data-toggle="modal" data-target={`#myModal${item.id}`}><img className="pic" src={require( `./assets/works/${item.imgName}`)} alt={item.altImg}/></a><br/>
      		<span>{item.title}</span><br/>
          <small><i>{item.description}</i></small>
      		<h5><u><a rel="noopener noreferrer" href={item.repo} title={item.title} target="_blank">Repo</a></u></h5>
      	</div>
      );
    });
    return (
      <div className="workBox col-xs-12">
      	{listItems}
      </div>
    );
  }
}

export default Work;