import React, { Component } from 'react';
//import {Link} from 'react-router';
import './css/Cite.css';
import data from './data/Cite.json';

class Cite extends Component {
  _selectLanguage() {
    return this.props.lang === "es" ? data[0] : data[1]
  }

  render() {
    const data = this._selectLanguage();
    return (
      <div className="col-xs-12 mainBox">
        <div className="col-xs-12 cite cite-box">
          <cite> {data.cite} </cite><br/>
          <small>-Stephen Hawking</small>
        </div>
      </div>
    );
  }
}

export default Cite;