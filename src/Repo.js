import React, { Component } from 'react';
import './css/Repo.css';

class Repo extends Component {
  render() {
    return (
      <div className="Repo-box">
        <h1>This is my Repo! {this.props.params.type}</h1>
      </div>
    );
  }
}

export default Repo;