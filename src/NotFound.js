import React, { Component } from 'react';
import './css/NotFound.css';

class NotFound extends Component {

  render() {
    return (
      <div className="NotFound-box">
      	<h1> Error 404 not found </h1>
      </div>
    );
  }
}

export default NotFound;