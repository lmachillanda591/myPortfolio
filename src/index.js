import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Router, Route, browserHistory, Redirect } from 'react-router'

import './css/index.css';
import App from './App';
import First from './First';
import Repo from './Repo';
import NotFound from './NotFound';


const render =
	(
  <Router history={browserHistory}>
    <Redirect from="/" to="/home/en"/>
    <Redirect from="/home" to="/home/en"/>
    <Route path="/" component={App}>
      <Route path="/home/:lang" component={First} />
      <Route path="/repo/:type" component={Repo} />
    </Route>
    <Route path="**" component={NotFound} />
  </Router>
)

ReactDOM.render(render, document.getElementById('root'));
registerServiceWorker();

//<a href="http://www.freepik.com/free-vector/abstract-polygonal-colorful-background_1176596.htm">Designed by Freepik</a> Poligonal atribute!