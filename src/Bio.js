import React, { Component } from 'react';
import './css/Bio.css';
import mePic from './assets/me.jpg';
import triforce from './assets/triforce.png';
import data from './data/bio.json';

class Bio extends Component {
  _selectLanguage() {
    return this.props.lang === "es" ? data[0] : data[1];
  }

  render() {
  	const data = this._selectLanguage();
    return (
      <div className="bioBox col-xs-12">
      	<p className="title">{data.title}</p>
      	<div className="col-xs-8 bioText">
      		<br/>
      		<div className="col-xs-12 col-md-6">
	      		<span> {data.line1} </span>
      		</div>
      		<div className="col-xs-12 col-md-6">
	      		<span> {data.line2} </span>
      		</div>
      		<div className="col-xs-12 col-md-12 lgText">
	      		<span> {data.line3} </span>
      		</div>
          <div className="col-xs-12 col-md-12 text-center">
            <span> Links: </span>
          </div>
          <div className="col-xs-12 col-md-12 text-center">
            <span>
              <a rel="noopener noreferrer" href="https://dev-io.herokuapp.com/#/videocall" title="WebRTC Example" target="_blank">
                WebRTC
              </a>
              <span> / </span>
              <a rel="noopener noreferrer" href="https://dev-io.herokuapp.com/#/editor" title="WebRTC Example" target="_blank">
                Editor
              </a>
              <span> / </span>
              <a rel="noopener noreferrer" href="https://gitlab.com/lmachillanda591" title="My Repo" target="_blank">
                My Repo
              </a>
            </span>
          </div>
      	</div>
      	<div className="text-center col-xs-4 bioImg">
      		<div className="bluCircle" alt="triforce">
            <img style={{position: 'absolute', width:'80%', height: '80%', left: '10%'}} src={triforce} alt="triforce"/>
          </div>
      		<img className="mePic" src={mePic} alt="Luis Machillanda"/>
      	</div>
      </div>
    );
  }
}

export default Bio;