import React, { Component } from 'react';
import {Link} from 'react-router';
import './css/Navbar.css';
import './data/global.js'

class Navbar extends Component {

  render() {
    return (
      <div>
        <ul>
          <li><Link to="/home/es" className="Navbar-links" activeClassName="active" title="home"><i className="glyphicon glyphicon-home"></i></Link></li>
          <li><Link to="/repo/ReatcJS" className="Navbar-links" activeClassName="active">Repo</Link></li>
          <li><Link to="/about_me" className="Navbar-links" activeClassName="active">About me</Link></li>
        </ul>
      </div>
    );
  }
}

export default Navbar;